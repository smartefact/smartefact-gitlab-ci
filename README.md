# Smartefact GitLab CI

[GitLab](https://gitlab.com/) CI templates.

## Changelog

This project does not maintain a changelog.

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is not licensed.
See [UNLICENSE](UNLICENSE).
